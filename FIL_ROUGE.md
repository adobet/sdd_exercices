Cours de structures de données de l'IUT d'Orléans. Conforme –on l'espère– au [PPN 2013](http://www.iut-informatique.fr/docs/ppn/fr.pdf?PHPSESSID=e803c697889f1283c297edfc3f9666da).

Ci-dessous, les notions à aborder, et les modalités.

Fils rouges
===========

Essayer d'avoir sur chaque feuille de TD/TP :

 * Un/des exercices naïfs (question de cours ou application directe du cours)
 * Un exercice faisant travailler la représentation de la mémoire
 * Un exercice posant des questions de performance d'un algo
 * Un exercice "défi"
 * Une fonction nécessitant une boucle while
 * Identifier clairement pour les étudiants ce qui est exigible à l'examen


Quelques compétences à cultiver au cours du module, sans qu'elles aient un moment dédié en cours magistral:

 * Organisation du code : découper en fonctions, réutiliser les fonctions déjà écrites
 * Documentation
 * Tests (utilisation via l'exerciseur; introduction à la conception? )
 * Typage
 * Justifier le fonctionnement de son code

Semaine par semaine
===================



Semaine 1 - nombres, chaines de caractères, listes et tuples
--------------------------------------------------------------

### Les *fonctions types* (cumul, min/max, vérification)

#. Présentation en cours
#. Approche de la notion de complexité (??)
#. Qcm


### Données mutables et immutables

#. Présentation en cours
#. Qcm


### Représentation de la mémoire

_Utiliser python tutor_ (démo en Cours). Introduire le mot « objet » pour les valeurs dans le tas (cf. python tutor).

#. Présentation en cours (pas indispensable)
#. Exercices naïfs en td / tp
#. Qcm

### Slicing (??)

_Présenter l'approche « numérotation des intervalles entre les valeurs » ; montrer les liens avec la représentation de la mémoire.

#. Pas de présentation en cours sauf s'il y a le temps
#. Exercices naïfs en td / tp
#. Qcm



Semaine 2 - ensembles et dictionnaires
----------------------------------------

### Ensembles

Nécessite données mutables et immutables; notation, méthodes; unicité des éléments. _Comparer la vitesse du `in` avec les listes_.


### Dictionnaires

Clés immutables, valeurs mutables; notation, méthodes; unicité des clés.


### Approche de la notion de complexité (Chronométrage)

`in` sur les listes vs les ensembles et dictionnaires, boucle vs accès par `[]` sur une liste. Parler de "boucles implicites".



Semaine 3 - structures imbriquées
----------------------------------------

_Semaine où on ne voit pas de fonction python nouvelle_

### Structures de données imbriquées

#. Exercices naïfs de "suivre les crochets" (légalité, type, valeur)

### Jointures

#. Introduire une représentation sous forme de graphes des clés / valeurs


### Algorithmique : applications du tri + complexité

Montrer les fonctions ``min``, ``max`` et ``sorted`` ??


Semaine 4 - Révisions
-----------------------

Insister sur les fils rouges en td et tp, puisqu'on a un peu de temps.

Introduire le get() en td / tp ? ☮



Semaine 5
---------

_DS_

td / tp: cf. semaine 4; commencer les notions de la semaine 6?



Semaine 6 - Programmation fonctionnelle
-----------------------------------------

On rattaque!

### Prog. fonctionnelle ☮☮

Les fonctions sont des objets comme les autres. Passage de fonctions en argument. Fonctions locales. Formulation de problèmes « complexes » par un choix judicieux de `key=`.

#. Utilisation de `max(truc, key=bidule)`
#. Utilisation de `sorted(truc, key=bidule)`

En td/tp: utilisations naïve de sorted(), min(), max()

### Listes triées

#. Utilisation de sorted()
#. Tri sur des listes de couples
#. Intérêt du tri comme précondition (ex: calcul du plus petit écart, avec chronométrage)


Semaine 7
---------

### Problèmes d'optimisation

Vocabulaire, exemples, intérêt du tri pour ces problèmes.

### Recherche dichotomique

Exemple d'utilisation du tri pour des raisons de performances

Semaine 8½
----------

Révisions + DS
