from twisted_fate import nombreDEtagesDuPlusHautChateau

entrees_visibles=[
        (23),
        (25),
        (26),
]
entrees_invisibles=[
        (2668),
        (2666),
        (2667),
]

@solution
def monNombreDEtagesDuPlusHautChateau(nombreDeCartes):
  return nombreDEtagesDuPlusHautChateau(nombreDeCartes)

