from hypothesis import given,example 
import hypothesis.strategies as st
from fail_test import fail_test

attendu_etudiant = [ 'total' ]

@solution
def total(l):
   return sum(l)

def close_enough(f1, f2, tolerance):
   if f1 == f2:
      return True # pour 0 et +/- infini
   a1 = abs(f1)
   a2 = abs(f2)
   return (abs(f1 - f2) / max(a1, a2)) < tolerance

@given(st.lists(st.floats(allow_infinity=False, allow_nan=False)))
@example([15,12,8,3])
def test_total_est_sum(code_etu, s):
   mon_total = sum(s)
   son_total = code_etu.total(s)
   if not close_enough(mon_total, son_total, 1e-5):
      fail_test("Sur l'entrée %s, vous retournez %s au lieu de %s" % (s, son_total, mon_total))
