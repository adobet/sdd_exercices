from hypothesis import given, example, assume
import hypothesis.strategies as st
from fail_test import fail_test
import math

attendu_etudiant = [ 'avoir' ]

def close_enough(f1, f2, tolerance):
   if f1 == f2:
      return True # pour 0 et +/- infini
   a1 = abs(f1)
   a2 = abs(f2)
   return (abs(f1 - f2) / max(a1, a2)) < tolerance

@given(st.dictionaries(st.text(min_size=5),
                       st.lists(st.floats(allow_nan=False, allow_infinity=False))))
@example({ 'Dominique': [15, 12, 8, 3],
           'Claude' : [20, 34],
	   'Mérédith' : [52],
	   'Alex' : [8]})
def test_avoir_ok(code_etu, s):
   if len(s):
      moyenne = sum(sum(d) for d in s.values()) / len(s)
      assume(math.isfinite(moyenne))
      for personne, depenses in s.items():
         a = code_etu.avoir(s, personne)
         if not close_enough(moyenne, sum(depenses) + a, 0.00001):
            failmsg = """Sur l'entrée:
            %r
            concernant %s:
            total_depenses(%f) + avoir(%f) ≠ dépense moyenne(%f)
            """
            fail_test(failmsg % (s, personne, sum(depenses), a, moyenne))

