import random
nb = list(range(100))
s="// question: 0  name: Switch category to $course$/top\n$CATEGORY: $course$/top\n\n// question: 0  name: Switch category to $course$/top/Défaut pour M1103 \n$CATEGORY: $course$/top/Défaut pour M1103 \n\n// question: 0  name: Switch category to $course$/top/Défaut pour M1103 /Tri-sorted\n$CATEGORY: $course$/top/Défaut pour M1103 /Tri-sorted\n\n// question: 0  name: Switch category to $course$/top/Défaut pour M1103 /Tri-sorted/tri de dictionnaires\n$CATEGORY: $course$/top/Défaut pour M1103 /Tri-sorted/tri de dictionnaires\n\n"

phrases = ["je suis ton père luke", "que la force soit avec toi", "you re talking to me", "i see dead people", "longue vie et prospérité", "chevron 7 enclenché", "leeloo dallas multipass", "vers l infini et au delà", "i will be back", "you shall not pass", "je s appelle groot", "aimes tu les films de gladiateur", "on en a gros"]

i=0
for p in phrases:
    i+=1
    l=p.split()
    d1={}
    d2={}
    for i in range(len(l)):
        d1[i] = l[i]
        d2[l[i]] = i
    o1=sorted(d1)
    o2=sorted(d2)
    p1=sorted(d1.items())
    p2=sorted(d2.items())
    q1=sorted(d1.items(), key = lambda x : x[1])
    q2=sorted(d2.items(), key = lambda x : x[1])
    #  random.shuffle(l)
    s+="// question: "+str(220345+i)+"  name: la fonction sorted()\n"+"::la fonction sorted()::[markdown]```\\ndico \\= "+str(d1)+"\\nessai \\= sorted(dico)\\n```\\nQuelle est la valeur de `essai` à l’issue de ce script ?{\n"+"=`"+str(o1)+"`\n"+"~`"+str(p1)+"`\n~`"+str(q1)+"`\n"+"~"+"`None`\n"+"~Rien de tout ça\n}\n\n"
    s+="// question: "+str(220345+i)+"  name: la fonction sorted()\n"+"::la fonction sorted()::[markdown]```\\ndico \\= "+str(d2)+"\\nessai \\= sorted(dico)\\n```\\nQuelle est la valeur de `essai` à l’issue de ce script ?{\n"+"=`"+str(o2)+"`\n"+"~`"+str(p2)+"`\n~`"+str(q2)+"`\n"+"~"+"`None`\n"+"~Rien de tout ça\n}\n\n"    
    
print(s)
