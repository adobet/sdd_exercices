import random

s="// question: 0  name: Switch category to $course$/top\n$CATEGORY: $course$/top\n\n// question: 0  name: Switch category to $course$/top/Défaut pour M1103 \n$CATEGORY: $course$/top/Défaut pour M1103 \n\n// question: 0  name: Switch category to $course$/top/Défaut pour M1103 /Tri-sorted avec key\n$CATEGORY: $course$/top/Défaut pour M1103 /Tri-sorted avec key\n\n// question: 0  name: Switch category to $course$/top/Défaut pour M1103 /Tri-sorted avec key/liste de tuples1\n$CATEGORY: $course$/top/Défaut pour M1103 /Tri-sorted avec key/liste de tuples1\n\n"

listes = [[],[],[],[]]

liste = list(range(5))+list(range(5))+list(range(5))
liste_s = 'a b c d e a b c d e a b c d e'.split()

for i in range(10):
    random.shuffle(liste_s)
    s1 = liste_s[:6]
    random.shuffle(liste_s)
    s2 = liste_s[:6]
    random.shuffle(liste)
    nb1 = liste[:6]
    random.shuffle(liste)
    nb2 = liste[:6]
    listes[0].append(list(zip(nb1, nb2)))
    listes[1].append(list(zip(s1, s2)))
    listes[2].append(list(zip(nb1, s2)))
    listes[3].append(list(zip(s1, nb2)))
    
i=0
for lliste in listes:
  for liste in lliste:
    n=0
    p, q1, q2, o1, o2, r = None, None, None, None, None, None
    bon = False
    while  (not bon) and n!=5:
        random.shuffle(liste)
        p=sorted(liste, key = lambda x : x[0])
        q1=sorted(sorted(liste, key = lambda x : x[1]), key = lambda x : x[0])
        q2=sorted(liste, key = lambda x : x[1])
        o1 = [ x for (x,_) in p]
        o2 = [ y for (_,y) in q1]
        r = [ y for (_,y) in p]
        bon = (p!=q1 and q2!=q1 and q2!=p and o1!= o2 and o1!=r  and o2 !=r)
        if bon:
            n=0
        else:
            n+=1
    if n!=5 :
        s+="// question: "+str(220345+i)+"  name: la fonction sorted()\n"+"::la fonction sorted()::[markdown]```\\ndef critere(tuple):\\n    return tuple[0]\nliste \\= "+str(liste)+"\\nessai \\= sorted(liste, key = critere)\\n```\\nQuelle est la valeur de `essai` à l’issue de ce script ?{\n"+"=`"+str(p)+"`\n"+"~`"+str(q1)+"`\n~`"+str(q2)+"`\n~`"+str(o1)+"`\n~`"+str(o2)+"`\n~`"+str(r)+"`\n~Rien de tout ça\n}\n\n"
print(s)
