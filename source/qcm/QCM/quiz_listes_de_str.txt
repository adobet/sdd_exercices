// question: 0  name: Switch category to $course$/top
$CATEGORY: $course$/top

// question: 0  name: Switch category to $course$/top/Défaut pour M1103 
$CATEGORY: $course$/top/Défaut pour M1103 

// question: 0  name: Switch category to $course$/top/Défaut pour M1103 /Tri-sorted
$CATEGORY: $course$/top/Défaut pour M1103 /Tri-sorted

// question: 0  name: Switch category to $course$/top/Défaut pour M1103 /Tri-sorted/listes de str
$CATEGORY: $course$/top/Défaut pour M1103 /Tri-sorted/listes de str

// question: 220346  name: la fonction sorted()
::la fonction sorted()::[markdown]```\nliste \= ['je', 'suis', 'ton', 'père', 'luke']\nessai \= sorted(liste)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`['je', 'luke', 'père', 'suis', 'ton']`
~`['je', 'ton', 'luke', 'suis', 'père']`
~`['je', 'suis', 'ton', 'père', 'luke']`
~`None`
~Rien de tout ça
}

// question: 220347  name: la fonction sorted()
::la fonction sorted()::[markdown]```\nliste \= ['que', 'la', 'force', 'soit', 'avec', 'toi']\nessai \= sorted(liste)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`['avec', 'force', 'la', 'que', 'soit', 'toi']`
~`['la', 'avec', 'que', 'force', 'soit', 'toi']`
~`['que', 'la', 'force', 'soit', 'avec', 'toi']`
~`None`
~Rien de tout ça
}

// question: 220348  name: la fonction sorted()
::la fonction sorted()::[markdown]```\nliste \= ['you', 're', 'talking', 'to', 'me']\nessai \= sorted(liste)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`['me', 're', 'talking', 'to', 'you']`
~`['to', 'me', 'you', 're', 'talking']`
~`['you', 're', 'talking', 'to', 'me']`
~`None`
~Rien de tout ça
}

// question: 220349  name: la fonction sorted()
::la fonction sorted()::[markdown]```\nliste \= ['i', 'see', 'dead', 'people']\nessai \= sorted(liste)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`['dead', 'i', 'people', 'see']`
~`['i', 'dead', 'people', 'see']`
~`['i', 'see', 'dead', 'people']`
~`None`
~Rien de tout ça
}

// question: 220350  name: la fonction sorted()
::la fonction sorted()::[markdown]```\nliste \= ['longue', 'vie', 'et', 'prospérité']\nessai \= sorted(liste)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`['et', 'longue', 'prospérité', 'vie']`
~`['longue', 'et', 'prospérité', 'vie']`
~`['longue', 'vie', 'et', 'prospérité']`
~`None`
~Rien de tout ça
}

// question: 220351  name: la fonction sorted()
::la fonction sorted()::[markdown]```\nliste \= ['chevron', '7', 'enclenché']\nessai \= sorted(liste)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`['7', 'chevron', 'enclenché']`
~`['enclenché', 'chevron', '7']`
~`['chevron', '7', 'enclenché']`
~`None`
~Rien de tout ça
}

// question: 220352  name: la fonction sorted()
::la fonction sorted()::[markdown]```\nliste \= ['leeloo', 'dallas', 'multipass']\nessai \= sorted(liste)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`['dallas', 'leeloo', 'multipass']`
~`['dallas', 'multipass', 'leeloo']`
~`['leeloo', 'dallas', 'multipass']`
~`None`
~Rien de tout ça
}

// question: 220353  name: la fonction sorted()
::la fonction sorted()::[markdown]```\nliste \= ['vers', 'l', 'infini', 'et', 'au', 'delà']\nessai \= sorted(liste)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`['au', 'delà', 'et', 'infini', 'l', 'vers']`
~`['et', 'infini', 'vers', 'delà', 'l', 'au']`
~`['vers', 'l', 'infini', 'et', 'au', 'delà']`
~`None`
~Rien de tout ça
}

// question: 220354  name: la fonction sorted()
::la fonction sorted()::[markdown]```\nliste \= ['i', 'will', 'be', 'back']\nessai \= sorted(liste)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`['back', 'be', 'i', 'will']`
~`['i', 'back', 'will', 'be']`
~`['i', 'will', 'be', 'back']`
~`None`
~Rien de tout ça
}

// question: 220355  name: la fonction sorted()
::la fonction sorted()::[markdown]```\nliste \= ['you', 'shall', 'not', 'pass']\nessai \= sorted(liste)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`['not', 'pass', 'shall', 'you']`
~`['shall', 'pass', 'you', 'not']`
~`['you', 'shall', 'not', 'pass']`
~`None`
~Rien de tout ça
}

// question: 220356  name: la fonction sorted()
::la fonction sorted()::[markdown]```\nliste \= ['je', 's', 'appelle', 'groot']\nessai \= sorted(liste)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`['appelle', 'groot', 'je', 's']`
~`['groot', 's', 'je', 'appelle']`
~`['je', 's', 'appelle', 'groot']`
~`None`
~Rien de tout ça
}

// question: 220357  name: la fonction sorted()
::la fonction sorted()::[markdown]```\nliste \= ['aimes', 'tu', 'les', 'films', 'de', 'gladiateur']\nessai \= sorted(liste)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`['aimes', 'de', 'films', 'gladiateur', 'les', 'tu']`
~`['tu', 'films', 'aimes', 'gladiateur', 'les', 'de']`
~`['aimes', 'tu', 'les', 'films', 'de', 'gladiateur']`
~`None`
~Rien de tout ça
}

// question: 220358  name: la fonction sorted()
::la fonction sorted()::[markdown]```\nliste \= ['on', 'en', 'a', 'gros']\nessai \= sorted(liste)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`['a', 'en', 'gros', 'on']`
~`['gros', 'a', 'en', 'on']`
~`['on', 'en', 'a', 'gros']`
~`None`
~Rien de tout ça
}


