// question: 0  name: Switch category to $course$/top
$CATEGORY: $course$/top

// question: 0  name: Switch category to $course$/top/Défaut pour M1103 
$CATEGORY: $course$/top/Défaut pour M1103 

// question: 0  name: Switch category to $course$/top/Défaut pour M1103 /Tri-sorted avec key
$CATEGORY: $course$/top/Défaut pour M1103 /Tri-sorted avec key

// question: 0  name: Switch category to $course$/top/Défaut pour M1103 /Tri-sorted avec key/liste de tuples2
$CATEGORY: $course$/top/Défaut pour M1103 /Tri-sorted avec key/liste de tuples2

// question: 220345  name: la fonction sorted()
::la fonction sorted()::[markdown]```\ndef critere(tuple):\n    return tuple[1]
liste \= [(4, 1), (0, 0), (2, 0), (2, 2), (1, 1), (3, 3)]\nessai \= sorted(liste, key = critere)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`[(0, 0), (2, 0), (4, 1), (1, 1), (2, 2), (3, 3)]`
~`[(0, 0), (2, 0), (1, 1), (4, 1), (2, 2), (3, 3)]`
~`[(0, 0), (1, 1), (2, 0), (2, 2), (3, 3), (4, 1)]`
~`[0, 0, 1, 1, 2, 3]`
~`[0, 2, 1, 4, 2, 3]`
~`[0, 2, 4, 1, 2, 3]`
~Rien de tout ça
}

// question: 220345  name: la fonction sorted()
::la fonction sorted()::[markdown]```\ndef critere(tuple):\n    return tuple[1]
liste \= [(0, 4), (2, 0), (3, 2), (1, 4), (0, 0), (3, 0)]\nessai \= sorted(liste, key = critere)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`[(2, 0), (0, 0), (3, 0), (3, 2), (0, 4), (1, 4)]`
~`[(0, 0), (2, 0), (3, 0), (3, 2), (0, 4), (1, 4)]`
~`[(0, 4), (0, 0), (1, 4), (2, 0), (3, 2), (3, 0)]`
~`[0, 0, 0, 2, 4, 4]`
~`[0, 2, 3, 3, 0, 1]`
~`[2, 0, 3, 3, 0, 1]`
~Rien de tout ça
}

// question: 220345  name: la fonction sorted()
::la fonction sorted()::[markdown]```\ndef critere(tuple):\n    return tuple[1]
liste \= [(0, 2), (3, 1), (1, 4), (0, 1), (0, 0), (2, 2)]\nessai \= sorted(liste, key = critere)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`[(0, 0), (3, 1), (0, 1), (0, 2), (2, 2), (1, 4)]`
~`[(0, 0), (0, 1), (3, 1), (0, 2), (2, 2), (1, 4)]`
~`[(0, 2), (0, 1), (0, 0), (1, 4), (2, 2), (3, 1)]`
~`[0, 1, 1, 2, 2, 4]`
~`[0, 0, 3, 0, 2, 1]`
~`[0, 3, 0, 0, 2, 1]`
~Rien de tout ça
}

// question: 220345  name: la fonction sorted()
::la fonction sorted()::[markdown]```\ndef critere(tuple):\n    return tuple[1]
liste \= [(1, 2), (1, 1), (3, 3), (1, 4), (0, 1), (3, 0)]\nessai \= sorted(liste, key = critere)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`[(3, 0), (1, 1), (0, 1), (1, 2), (3, 3), (1, 4)]`
~`[(3, 0), (0, 1), (1, 1), (1, 2), (3, 3), (1, 4)]`
~`[(0, 1), (1, 2), (1, 1), (1, 4), (3, 3), (3, 0)]`
~`[0, 1, 1, 2, 3, 4]`
~`[3, 0, 1, 1, 3, 1]`
~`[3, 1, 0, 1, 3, 1]`
~Rien de tout ça
}

// question: 220345  name: la fonction sorted()
::la fonction sorted()::[markdown]```\ndef critere(tuple):\n    return tuple[1]
liste \= [(0, 0), (3, 0), (2, 3), (1, 2), (0, 0), (1, 3)]\nessai \= sorted(liste, key = critere)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`[(0, 0), (3, 0), (0, 0), (1, 2), (2, 3), (1, 3)]`
~`[(0, 0), (0, 0), (3, 0), (1, 2), (1, 3), (2, 3)]`
~`[(0, 0), (0, 0), (1, 2), (1, 3), (2, 3), (3, 0)]`
~`[0, 0, 0, 2, 3, 3]`
~`[0, 0, 3, 1, 1, 2]`
~`[0, 3, 0, 1, 2, 1]`
~Rien de tout ça
}

// question: 220345  name: la fonction sorted()
::la fonction sorted()::[markdown]```\ndef critere(tuple):\n    return tuple[1]
liste \= [(0, 3), (0, 0), (4, 4), (3, 0), (4, 2), (2, 2)]\nessai \= sorted(liste, key = critere)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`[(0, 0), (3, 0), (4, 2), (2, 2), (0, 3), (4, 4)]`
~`[(0, 0), (3, 0), (2, 2), (4, 2), (0, 3), (4, 4)]`
~`[(0, 3), (0, 0), (2, 2), (3, 0), (4, 4), (4, 2)]`
~`[0, 0, 2, 2, 3, 4]`
~`[0, 3, 2, 4, 0, 4]`
~`[0, 3, 4, 2, 0, 4]`
~Rien de tout ça
}

// question: 220345  name: la fonction sorted()
::la fonction sorted()::[markdown]```\ndef critere(tuple):\n    return tuple[1]
liste \= [(1, 1), (1, 2), (4, 0), (0, 1), (0, 3), (1, 4)]\nessai \= sorted(liste, key = critere)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`[(4, 0), (1, 1), (0, 1), (1, 2), (0, 3), (1, 4)]`
~`[(4, 0), (0, 1), (1, 1), (1, 2), (0, 3), (1, 4)]`
~`[(0, 1), (0, 3), (1, 1), (1, 2), (1, 4), (4, 0)]`
~`[0, 1, 1, 2, 3, 4]`
~`[4, 0, 1, 1, 0, 1]`
~`[4, 1, 0, 1, 0, 1]`
~Rien de tout ça
}

// question: 220345  name: la fonction sorted()
::la fonction sorted()::[markdown]```\ndef critere(tuple):\n    return tuple[1]
liste \= [(4, 2), (4, 4), (2, 1), (0, 0), (2, 2), (3, 4)]\nessai \= sorted(liste, key = critere)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`[(0, 0), (2, 1), (4, 2), (2, 2), (4, 4), (3, 4)]`
~`[(0, 0), (2, 1), (2, 2), (4, 2), (3, 4), (4, 4)]`
~`[(0, 0), (2, 1), (2, 2), (3, 4), (4, 2), (4, 4)]`
~`[0, 1, 2, 2, 4, 4]`
~`[0, 2, 2, 4, 3, 4]`
~`[0, 2, 4, 2, 4, 3]`
~Rien de tout ça
}

// question: 220345  name: la fonction sorted()
::la fonction sorted()::[markdown]```\ndef critere(tuple):\n    return tuple[1]
liste \= [(4, 4), (1, 0), (1, 3), (0, 4), (3, 0), (3, 1)]\nessai \= sorted(liste, key = critere)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`[(1, 0), (3, 0), (3, 1), (1, 3), (4, 4), (0, 4)]`
~`[(1, 0), (3, 0), (3, 1), (1, 3), (0, 4), (4, 4)]`
~`[(0, 4), (1, 0), (1, 3), (3, 0), (3, 1), (4, 4)]`
~`[0, 0, 1, 3, 4, 4]`
~`[1, 3, 3, 1, 0, 4]`
~`[1, 3, 3, 1, 4, 0]`
~Rien de tout ça
}

// question: 220345  name: la fonction sorted()
::la fonction sorted()::[markdown]```\ndef critere(tuple):\n    return tuple[1]
liste \= [('c', 'c'), ('e', 'd'), ('b', 'e'), ('d', 'd'), ('b', 'a'), ('d', 'c')]\nessai \= sorted(liste, key = critere)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`[('b', 'a'), ('c', 'c'), ('d', 'c'), ('e', 'd'), ('d', 'd'), ('b', 'e')]`
~`[('b', 'a'), ('c', 'c'), ('d', 'c'), ('d', 'd'), ('e', 'd'), ('b', 'e')]`
~`[('b', 'e'), ('b', 'a'), ('c', 'c'), ('d', 'd'), ('d', 'c'), ('e', 'd')]`
~`['a', 'c', 'c', 'd', 'd', 'e']`
~`['b', 'c', 'd', 'd', 'e', 'b']`
~`['b', 'c', 'd', 'e', 'd', 'b']`
~Rien de tout ça
}

// question: 220345  name: la fonction sorted()
::la fonction sorted()::[markdown]```\ndef critere(tuple):\n    return tuple[1]
liste \= [('e', 'c'), ('d', 'd'), ('c', 'c'), ('d', 'b'), ('b', 'a'), ('e', 'a')]\nessai \= sorted(liste, key = critere)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`[('b', 'a'), ('e', 'a'), ('d', 'b'), ('e', 'c'), ('c', 'c'), ('d', 'd')]`
~`[('b', 'a'), ('e', 'a'), ('d', 'b'), ('c', 'c'), ('e', 'c'), ('d', 'd')]`
~`[('b', 'a'), ('c', 'c'), ('d', 'd'), ('d', 'b'), ('e', 'c'), ('e', 'a')]`
~`['a', 'a', 'b', 'c', 'c', 'd']`
~`['b', 'e', 'd', 'c', 'e', 'd']`
~`['b', 'e', 'd', 'e', 'c', 'd']`
~Rien de tout ça
}

// question: 220345  name: la fonction sorted()
::la fonction sorted()::[markdown]```\ndef critere(tuple):\n    return tuple[1]
liste \= [('e', 'a'), ('b', 'a'), ('a', 'b'), ('d', 'e'), ('c', 'e'), ('e', 'a')]\nessai \= sorted(liste, key = critere)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`[('e', 'a'), ('b', 'a'), ('e', 'a'), ('a', 'b'), ('d', 'e'), ('c', 'e')]`
~`[('b', 'a'), ('e', 'a'), ('e', 'a'), ('a', 'b'), ('c', 'e'), ('d', 'e')]`
~`[('a', 'b'), ('b', 'a'), ('c', 'e'), ('d', 'e'), ('e', 'a'), ('e', 'a')]`
~`['a', 'a', 'a', 'b', 'e', 'e']`
~`['b', 'e', 'e', 'a', 'c', 'd']`
~`['e', 'b', 'e', 'a', 'd', 'c']`
~Rien de tout ça
}

// question: 220345  name: la fonction sorted()
::la fonction sorted()::[markdown]```\ndef critere(tuple):\n    return tuple[1]
liste \= [('e', 'd'), ('b', 'e'), ('c', 'b'), ('c', 'a'), ('c', 'a'), ('b', 'd')]\nessai \= sorted(liste, key = critere)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`[('c', 'a'), ('c', 'a'), ('c', 'b'), ('e', 'd'), ('b', 'd'), ('b', 'e')]`
~`[('c', 'a'), ('c', 'a'), ('c', 'b'), ('b', 'd'), ('e', 'd'), ('b', 'e')]`
~`[('b', 'e'), ('b', 'd'), ('c', 'b'), ('c', 'a'), ('c', 'a'), ('e', 'd')]`
~`['a', 'a', 'b', 'd', 'd', 'e']`
~`['c', 'c', 'c', 'b', 'e', 'b']`
~`['c', 'c', 'c', 'e', 'b', 'b']`
~Rien de tout ça
}

// question: 220345  name: la fonction sorted()
::la fonction sorted()::[markdown]```\ndef critere(tuple):\n    return tuple[1]
liste \= [('b', 'a'), ('e', 'b'), ('a', 'e'), ('d', 'd'), ('a', 'a'), ('d', 'd')]\nessai \= sorted(liste, key = critere)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`[('b', 'a'), ('a', 'a'), ('e', 'b'), ('d', 'd'), ('d', 'd'), ('a', 'e')]`
~`[('a', 'a'), ('b', 'a'), ('e', 'b'), ('d', 'd'), ('d', 'd'), ('a', 'e')]`
~`[('a', 'e'), ('a', 'a'), ('b', 'a'), ('d', 'd'), ('d', 'd'), ('e', 'b')]`
~`['a', 'a', 'b', 'd', 'd', 'e']`
~`['a', 'b', 'e', 'd', 'd', 'a']`
~`['b', 'a', 'e', 'd', 'd', 'a']`
~Rien de tout ça
}

// question: 220345  name: la fonction sorted()
::la fonction sorted()::[markdown]```\ndef critere(tuple):\n    return tuple[1]
liste \= [('e', 'c'), ('d', 'b'), ('e', 'a'), ('c', 'a'), ('b', 'b'), ('a', 'e')]\nessai \= sorted(liste, key = critere)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`[('e', 'a'), ('c', 'a'), ('d', 'b'), ('b', 'b'), ('e', 'c'), ('a', 'e')]`
~`[('c', 'a'), ('e', 'a'), ('b', 'b'), ('d', 'b'), ('e', 'c'), ('a', 'e')]`
~`[('a', 'e'), ('b', 'b'), ('c', 'a'), ('d', 'b'), ('e', 'c'), ('e', 'a')]`
~`['a', 'a', 'b', 'b', 'c', 'e']`
~`['c', 'e', 'b', 'd', 'e', 'a']`
~`['e', 'c', 'd', 'b', 'e', 'a']`
~Rien de tout ça
}

// question: 220345  name: la fonction sorted()
::la fonction sorted()::[markdown]```\ndef critere(tuple):\n    return tuple[1]
liste \= [('e', 'e'), ('c', 'd'), ('d', 'a'), ('b', 'd'), ('b', 'd'), ('d', 'a')]\nessai \= sorted(liste, key = critere)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`[('d', 'a'), ('d', 'a'), ('c', 'd'), ('b', 'd'), ('b', 'd'), ('e', 'e')]`
~`[('d', 'a'), ('d', 'a'), ('b', 'd'), ('b', 'd'), ('c', 'd'), ('e', 'e')]`
~`[('b', 'd'), ('b', 'd'), ('c', 'd'), ('d', 'a'), ('d', 'a'), ('e', 'e')]`
~`['a', 'a', 'd', 'd', 'd', 'e']`
~`['d', 'd', 'b', 'b', 'c', 'e']`
~`['d', 'd', 'c', 'b', 'b', 'e']`
~Rien de tout ça
}

// question: 220345  name: la fonction sorted()
::la fonction sorted()::[markdown]```\ndef critere(tuple):\n    return tuple[1]
liste \= [('e', 'c'), ('d', 'd'), ('d', 'b'), ('c', 'a'), ('b', 'c'), ('d', 'd')]\nessai \= sorted(liste, key = critere)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`[('c', 'a'), ('d', 'b'), ('e', 'c'), ('b', 'c'), ('d', 'd'), ('d', 'd')]`
~`[('c', 'a'), ('d', 'b'), ('b', 'c'), ('e', 'c'), ('d', 'd'), ('d', 'd')]`
~`[('b', 'c'), ('c', 'a'), ('d', 'd'), ('d', 'b'), ('d', 'd'), ('e', 'c')]`
~`['a', 'b', 'c', 'c', 'd', 'd']`
~`['c', 'd', 'b', 'e', 'd', 'd']`
~`['c', 'd', 'e', 'b', 'd', 'd']`
~Rien de tout ça
}

// question: 220345  name: la fonction sorted()
::la fonction sorted()::[markdown]```\ndef critere(tuple):\n    return tuple[1]
liste \= [('e', 'a'), ('d', 'c'), ('d', 'e'), ('b', 'a'), ('b', 'b'), ('d', 'd')]\nessai \= sorted(liste, key = critere)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`[('e', 'a'), ('b', 'a'), ('b', 'b'), ('d', 'c'), ('d', 'd'), ('d', 'e')]`
~`[('b', 'a'), ('e', 'a'), ('b', 'b'), ('d', 'c'), ('d', 'd'), ('d', 'e')]`
~`[('b', 'a'), ('b', 'b'), ('d', 'c'), ('d', 'e'), ('d', 'd'), ('e', 'a')]`
~`['a', 'a', 'b', 'c', 'd', 'e']`
~`['b', 'e', 'b', 'd', 'd', 'd']`
~`['e', 'b', 'b', 'd', 'd', 'd']`
~Rien de tout ça
}

// question: 220345  name: la fonction sorted()
::la fonction sorted()::[markdown]```\ndef critere(tuple):\n    return tuple[1]
liste \= [('d', 'a'), ('e', 'a'), ('e', 'e'), ('b', 'e'), ('d', 'c'), ('c', 'd')]\nessai \= sorted(liste, key = critere)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`[('d', 'a'), ('e', 'a'), ('d', 'c'), ('c', 'd'), ('e', 'e'), ('b', 'e')]`
~`[('d', 'a'), ('e', 'a'), ('d', 'c'), ('c', 'd'), ('b', 'e'), ('e', 'e')]`
~`[('b', 'e'), ('c', 'd'), ('d', 'a'), ('d', 'c'), ('e', 'a'), ('e', 'e')]`
~`['a', 'a', 'c', 'd', 'e', 'e']`
~`['d', 'e', 'd', 'c', 'b', 'e']`
~`['d', 'e', 'd', 'c', 'e', 'b']`
~Rien de tout ça
}

// question: 220345  name: la fonction sorted()
::la fonction sorted()::[markdown]```\ndef critere(tuple):\n    return tuple[1]
liste \= [(2, 'e'), (4, 'a'), (0, 'd'), (3, 'c'), (1, 'd'), (2, 'c')]\nessai \= sorted(liste, key = critere)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`[(4, 'a'), (3, 'c'), (2, 'c'), (0, 'd'), (1, 'd'), (2, 'e')]`
~`[(4, 'a'), (2, 'c'), (3, 'c'), (0, 'd'), (1, 'd'), (2, 'e')]`
~`[(0, 'd'), (1, 'd'), (2, 'e'), (2, 'c'), (3, 'c'), (4, 'a')]`
~`['a', 'c', 'c', 'd', 'd', 'e']`
~`[4, 2, 3, 0, 1, 2]`
~`[4, 3, 2, 0, 1, 2]`
~Rien de tout ça
}

// question: 220345  name: la fonction sorted()
::la fonction sorted()::[markdown]```\ndef critere(tuple):\n    return tuple[1]
liste \= [(3, 'c'), (0, 'a'), (0, 'd'), (1, 'b'), (3, 'a'), (2, 'c')]\nessai \= sorted(liste, key = critere)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`[(0, 'a'), (3, 'a'), (1, 'b'), (3, 'c'), (2, 'c'), (0, 'd')]`
~`[(0, 'a'), (3, 'a'), (1, 'b'), (2, 'c'), (3, 'c'), (0, 'd')]`
~`[(0, 'a'), (0, 'd'), (1, 'b'), (2, 'c'), (3, 'c'), (3, 'a')]`
~`['a', 'a', 'b', 'c', 'c', 'd']`
~`[0, 3, 1, 2, 3, 0]`
~`[0, 3, 1, 3, 2, 0]`
~Rien de tout ça
}

// question: 220345  name: la fonction sorted()
::la fonction sorted()::[markdown]```\ndef critere(tuple):\n    return tuple[1]
liste \= [(0, 'e'), (3, 'a'), (0, 'e'), (0, 'a'), (1, 'b'), (2, 'a')]\nessai \= sorted(liste, key = critere)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`[(3, 'a'), (0, 'a'), (2, 'a'), (1, 'b'), (0, 'e'), (0, 'e')]`
~`[(0, 'a'), (2, 'a'), (3, 'a'), (1, 'b'), (0, 'e'), (0, 'e')]`
~`[(0, 'e'), (0, 'e'), (0, 'a'), (1, 'b'), (2, 'a'), (3, 'a')]`
~`['a', 'a', 'a', 'b', 'e', 'e']`
~`[0, 2, 3, 1, 0, 0]`
~`[3, 0, 2, 1, 0, 0]`
~Rien de tout ça
}

// question: 220345  name: la fonction sorted()
::la fonction sorted()::[markdown]```\ndef critere(tuple):\n    return tuple[1]
liste \= [(3, 'd'), (0, 'd'), (1, 'e'), (1, 'b'), (3, 'a'), (1, 'a')]\nessai \= sorted(liste, key = critere)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`[(3, 'a'), (1, 'a'), (1, 'b'), (3, 'd'), (0, 'd'), (1, 'e')]`
~`[(1, 'a'), (3, 'a'), (1, 'b'), (0, 'd'), (3, 'd'), (1, 'e')]`
~`[(0, 'd'), (1, 'e'), (1, 'b'), (1, 'a'), (3, 'd'), (3, 'a')]`
~`['a', 'a', 'b', 'd', 'd', 'e']`
~`[1, 3, 1, 0, 3, 1]`
~`[3, 1, 1, 3, 0, 1]`
~Rien de tout ça
}

// question: 220345  name: la fonction sorted()
::la fonction sorted()::[markdown]```\ndef critere(tuple):\n    return tuple[1]
liste \= [(2, 'b'), (0, 'd'), (3, 'a'), (0, 'd'), (1, 'a'), (1, 'e')]\nessai \= sorted(liste, key = critere)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`[(3, 'a'), (1, 'a'), (2, 'b'), (0, 'd'), (0, 'd'), (1, 'e')]`
~`[(1, 'a'), (3, 'a'), (2, 'b'), (0, 'd'), (0, 'd'), (1, 'e')]`
~`[(0, 'd'), (0, 'd'), (1, 'a'), (1, 'e'), (2, 'b'), (3, 'a')]`
~`['a', 'a', 'b', 'd', 'd', 'e']`
~`[1, 3, 2, 0, 0, 1]`
~`[3, 1, 2, 0, 0, 1]`
~Rien de tout ça
}

// question: 220345  name: la fonction sorted()
::la fonction sorted()::[markdown]```\ndef critere(tuple):\n    return tuple[1]
liste \= [(4, 'b'), (4, 'b'), (3, 'a'), (0, 'c'), (0, 'a'), (2, 'e')]\nessai \= sorted(liste, key = critere)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`[(3, 'a'), (0, 'a'), (4, 'b'), (4, 'b'), (0, 'c'), (2, 'e')]`
~`[(0, 'a'), (3, 'a'), (4, 'b'), (4, 'b'), (0, 'c'), (2, 'e')]`
~`[(0, 'c'), (0, 'a'), (2, 'e'), (3, 'a'), (4, 'b'), (4, 'b')]`
~`['a', 'a', 'b', 'b', 'c', 'e']`
~`[0, 3, 4, 4, 0, 2]`
~`[3, 0, 4, 4, 0, 2]`
~Rien de tout ça
}

// question: 220345  name: la fonction sorted()
::la fonction sorted()::[markdown]```\ndef critere(tuple):\n    return tuple[1]
liste \= [(2, 'd'), (4, 'a'), (3, 'a'), (3, 'd'), (0, 'd'), (0, 'e')]\nessai \= sorted(liste, key = critere)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`[(4, 'a'), (3, 'a'), (2, 'd'), (3, 'd'), (0, 'd'), (0, 'e')]`
~`[(3, 'a'), (4, 'a'), (0, 'd'), (2, 'd'), (3, 'd'), (0, 'e')]`
~`[(0, 'd'), (0, 'e'), (2, 'd'), (3, 'a'), (3, 'd'), (4, 'a')]`
~`['a', 'a', 'd', 'd', 'd', 'e']`
~`[3, 4, 0, 2, 3, 0]`
~`[4, 3, 2, 3, 0, 0]`
~Rien de tout ça
}

// question: 220345  name: la fonction sorted()
::la fonction sorted()::[markdown]```\ndef critere(tuple):\n    return tuple[1]
liste \= [(1, 'c'), (1, 'd'), (0, 'd'), (1, 'b'), (4, 'a'), (0, 'c')]\nessai \= sorted(liste, key = critere)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`[(4, 'a'), (1, 'b'), (1, 'c'), (0, 'c'), (1, 'd'), (0, 'd')]`
~`[(4, 'a'), (1, 'b'), (0, 'c'), (1, 'c'), (0, 'd'), (1, 'd')]`
~`[(0, 'd'), (0, 'c'), (1, 'c'), (1, 'd'), (1, 'b'), (4, 'a')]`
~`['a', 'b', 'c', 'c', 'd', 'd']`
~`[4, 1, 0, 1, 0, 1]`
~`[4, 1, 1, 0, 1, 0]`
~Rien de tout ça
}

// question: 220345  name: la fonction sorted()
::la fonction sorted()::[markdown]```\ndef critere(tuple):\n    return tuple[1]
liste \= [(0, 'c'), (4, 'a'), (2, 'b'), (4, 'd'), (3, 'a'), (2, 'e')]\nessai \= sorted(liste, key = critere)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`[(4, 'a'), (3, 'a'), (2, 'b'), (0, 'c'), (4, 'd'), (2, 'e')]`
~`[(3, 'a'), (4, 'a'), (2, 'b'), (0, 'c'), (4, 'd'), (2, 'e')]`
~`[(0, 'c'), (2, 'b'), (2, 'e'), (3, 'a'), (4, 'a'), (4, 'd')]`
~`['a', 'a', 'b', 'c', 'd', 'e']`
~`[3, 4, 2, 0, 4, 2]`
~`[4, 3, 2, 0, 4, 2]`
~Rien de tout ça
}

// question: 220345  name: la fonction sorted()
::la fonction sorted()::[markdown]```\ndef critere(tuple):\n    return tuple[1]
liste \= [(3, 'a'), (1, 'a'), (4, 'c'), (0, 'd'), (3, 'e'), (1, 'e')]\nessai \= sorted(liste, key = critere)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`[(3, 'a'), (1, 'a'), (4, 'c'), (0, 'd'), (3, 'e'), (1, 'e')]`
~`[(1, 'a'), (3, 'a'), (4, 'c'), (0, 'd'), (1, 'e'), (3, 'e')]`
~`[(0, 'd'), (1, 'a'), (1, 'e'), (3, 'a'), (3, 'e'), (4, 'c')]`
~`['a', 'a', 'c', 'd', 'e', 'e']`
~`[1, 3, 4, 0, 1, 3]`
~`[3, 1, 4, 0, 3, 1]`
~Rien de tout ça
}

// question: 220345  name: la fonction sorted()
::la fonction sorted()::[markdown]```\ndef critere(tuple):\n    return tuple[1]
liste \= [('b', 2), ('c', 0), ('e', 1), ('d', 3), ('d', 0), ('b', 1)]\nessai \= sorted(liste, key = critere)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`[('c', 0), ('d', 0), ('e', 1), ('b', 1), ('b', 2), ('d', 3)]`
~`[('c', 0), ('d', 0), ('b', 1), ('e', 1), ('b', 2), ('d', 3)]`
~`[('b', 2), ('b', 1), ('c', 0), ('d', 3), ('d', 0), ('e', 1)]`
~`[0, 0, 1, 1, 2, 3]`
~`['c', 'd', 'b', 'e', 'b', 'd']`
~`['c', 'd', 'e', 'b', 'b', 'd']`
~Rien de tout ça
}

// question: 220345  name: la fonction sorted()
::la fonction sorted()::[markdown]```\ndef critere(tuple):\n    return tuple[1]
liste \= [('e', 2), ('b', 0), ('d', 0), ('c', 0), ('d', 4), ('e', 4)]\nessai \= sorted(liste, key = critere)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`[('b', 0), ('d', 0), ('c', 0), ('e', 2), ('d', 4), ('e', 4)]`
~`[('b', 0), ('c', 0), ('d', 0), ('e', 2), ('d', 4), ('e', 4)]`
~`[('b', 0), ('c', 0), ('d', 0), ('d', 4), ('e', 2), ('e', 4)]`
~`[0, 0, 0, 2, 4, 4]`
~`['b', 'c', 'd', 'e', 'd', 'e']`
~`['b', 'd', 'c', 'e', 'd', 'e']`
~Rien de tout ça
}

// question: 220345  name: la fonction sorted()
::la fonction sorted()::[markdown]```\ndef critere(tuple):\n    return tuple[1]
liste \= [('c', 1), ('b', 1), ('d', 2), ('e', 2), ('e', 0), ('a', 4)]\nessai \= sorted(liste, key = critere)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`[('e', 0), ('c', 1), ('b', 1), ('d', 2), ('e', 2), ('a', 4)]`
~`[('e', 0), ('b', 1), ('c', 1), ('d', 2), ('e', 2), ('a', 4)]`
~`[('a', 4), ('b', 1), ('c', 1), ('d', 2), ('e', 2), ('e', 0)]`
~`[0, 1, 1, 2, 2, 4]`
~`['e', 'b', 'c', 'd', 'e', 'a']`
~`['e', 'c', 'b', 'd', 'e', 'a']`
~Rien de tout ça
}

// question: 220345  name: la fonction sorted()
::la fonction sorted()::[markdown]```\ndef critere(tuple):\n    return tuple[1]
liste \= [('b', 0), ('c', 3), ('e', 1), ('b', 4), ('c', 2), ('c', 1)]\nessai \= sorted(liste, key = critere)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`[('b', 0), ('e', 1), ('c', 1), ('c', 2), ('c', 3), ('b', 4)]`
~`[('b', 0), ('c', 1), ('e', 1), ('c', 2), ('c', 3), ('b', 4)]`
~`[('b', 0), ('b', 4), ('c', 3), ('c', 2), ('c', 1), ('e', 1)]`
~`[0, 1, 1, 2, 3, 4]`
~`['b', 'c', 'e', 'c', 'c', 'b']`
~`['b', 'e', 'c', 'c', 'c', 'b']`
~Rien de tout ça
}

// question: 220345  name: la fonction sorted()
::la fonction sorted()::[markdown]```\ndef critere(tuple):\n    return tuple[1]
liste \= [('e', 3), ('d', 0), ('d', 0), ('a', 3), ('a', 0), ('b', 2)]\nessai \= sorted(liste, key = critere)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`[('d', 0), ('d', 0), ('a', 0), ('b', 2), ('e', 3), ('a', 3)]`
~`[('a', 0), ('d', 0), ('d', 0), ('b', 2), ('a', 3), ('e', 3)]`
~`[('a', 3), ('a', 0), ('b', 2), ('d', 0), ('d', 0), ('e', 3)]`
~`[0, 0, 0, 2, 3, 3]`
~`['a', 'd', 'd', 'b', 'a', 'e']`
~`['d', 'd', 'a', 'b', 'e', 'a']`
~Rien de tout ça
}

// question: 220345  name: la fonction sorted()
::la fonction sorted()::[markdown]```\ndef critere(tuple):\n    return tuple[1]
liste \= [('d', 2), ('e', 3), ('b', 4), ('e', 0), ('c', 0), ('a', 2)]\nessai \= sorted(liste, key = critere)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`[('e', 0), ('c', 0), ('d', 2), ('a', 2), ('e', 3), ('b', 4)]`
~`[('c', 0), ('e', 0), ('a', 2), ('d', 2), ('e', 3), ('b', 4)]`
~`[('a', 2), ('b', 4), ('c', 0), ('d', 2), ('e', 3), ('e', 0)]`
~`[0, 0, 2, 2, 3, 4]`
~`['c', 'e', 'a', 'd', 'e', 'b']`
~`['e', 'c', 'd', 'a', 'e', 'b']`
~Rien de tout ça
}

// question: 220345  name: la fonction sorted()
::la fonction sorted()::[markdown]```\ndef critere(tuple):\n    return tuple[1]
liste \= [('e', 0), ('c', 3), ('d', 4), ('b', 1), ('b', 0), ('d', 2)]\nessai \= sorted(liste, key = critere)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`[('e', 0), ('b', 0), ('b', 1), ('d', 2), ('c', 3), ('d', 4)]`
~`[('b', 0), ('e', 0), ('b', 1), ('d', 2), ('c', 3), ('d', 4)]`
~`[('b', 1), ('b', 0), ('c', 3), ('d', 4), ('d', 2), ('e', 0)]`
~`[0, 0, 1, 2, 3, 4]`
~`['b', 'e', 'b', 'd', 'c', 'd']`
~`['e', 'b', 'b', 'd', 'c', 'd']`
~Rien de tout ça
}

// question: 220345  name: la fonction sorted()
::la fonction sorted()::[markdown]```\ndef critere(tuple):\n    return tuple[1]
liste \= [('d', 0), ('b', 2), ('b', 2), ('d', 1), ('e', 4), ('d', 4)]\nessai \= sorted(liste, key = critere)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`[('d', 0), ('d', 1), ('b', 2), ('b', 2), ('e', 4), ('d', 4)]`
~`[('d', 0), ('d', 1), ('b', 2), ('b', 2), ('d', 4), ('e', 4)]`
~`[('b', 2), ('b', 2), ('d', 0), ('d', 1), ('d', 4), ('e', 4)]`
~`[0, 1, 2, 2, 4, 4]`
~`['d', 'd', 'b', 'b', 'd', 'e']`
~`['d', 'd', 'b', 'b', 'e', 'd']`
~Rien de tout ça
}

// question: 220345  name: la fonction sorted()
::la fonction sorted()::[markdown]```\ndef critere(tuple):\n    return tuple[1]
liste \= [('b', 0), ('d', 4), ('e', 0), ('c', 4), ('e', 3), ('d', 1)]\nessai \= sorted(liste, key = critere)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
=`[('b', 0), ('e', 0), ('d', 1), ('e', 3), ('d', 4), ('c', 4)]`
~`[('b', 0), ('e', 0), ('d', 1), ('e', 3), ('c', 4), ('d', 4)]`
~`[('b', 0), ('c', 4), ('d', 4), ('d', 1), ('e', 0), ('e', 3)]`
~`[0, 0, 1, 3, 4, 4]`
~`['b', 'e', 'd', 'e', 'c', 'd']`
~`['b', 'e', 'd', 'e', 'd', 'c']`
~Rien de tout ça
}


