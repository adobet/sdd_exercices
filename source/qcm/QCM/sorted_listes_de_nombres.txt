// question: 0  name: Switch category to $course$/top
$CATEGORY: $course$/top


// question: 0  name: Switch category to $course$/top/Défaut pour M1103 
$CATEGORY: $course$/top/Défaut pour M1103 


// question: 0  name: Switch category to $course$/top/Défaut pour M1103 /Tri-sorted - questions de cours
$CATEGORY: $course$/top/Défaut pour M1103 /Tri-sorted


// question: 0  name: Switch category to $course$/top/Défaut pour M1103 /Tri-sorted - questions de cours/questions1
$CATEGORY: $course$/top/Défaut pour M1103 /Tri-sorted/listes de nombres


// question: 220338  name: la fonction sorted()
::la fonction sorted()::[markdown]```\nliste \= [5, 6, 8, 1, 42, 7, 6]\nessai \= sorted(liste)\n```\nQuelle est la valeur de `essai` à l’issue de ce script ?{
	=`[1, 5, 6, 6, 7, 8, 42]`
	~`[5, 6, 8, 1, 42, 7, 6]`
	~`[42, 8, 7, 6, 6, 5, 1]`#La fonction `sorted()` trie par défaut dans l\'ordre croissant
	~`None`
	~Rien de tout ça
}


