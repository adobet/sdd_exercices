import random
nb = list(range(100))
s="// question: 0  name: Switch category to $course$/top\n$CATEGORY: $course$/top\n\n// question: 0  name: Switch category to $course$/top/Défaut pour M1103 \n$CATEGORY: $course$/top/Défaut pour M1103 \n\n// question: 0  name: Switch category to $course$/top/Défaut pour M1103 /Tri-sorted\n$CATEGORY: $course$/top/Défaut pour M1103 /Tri-sorted\n\n// question: 0  name: Switch category to $course$/top/Défaut pour M1103 /Tri-sorted/listes de nombres\n$CATEGORY: $course$/top/Défaut pour M1103 /Tri-sorted/listes de nombres\n\n"


for i in range(40):
    random.shuffle(nb)
    l = nb[:7]
    if l != sorted(l):
        s+="// question: "+str(220345+i)+"  name: la fonction sorted()\n"+"::la fonction sorted()::[markdown]```\\nliste \\= "+str(l)+"\\nessai \\= sorted(liste)\\n```\\nQuelle est la valeur de `essai` à l’issue de ce script ?{\n"+"=`"+str(sorted(l))+"`\n"+"~`"+str(l)+"`\n"+"~`"+str(sorted(l, reverse=True))+"`#La fonction `sorted()` trie par défaut dans l'ordre croissant\n"+"~`None`\n"+"~Rien de tout ça\n}\n\n"
    
print(s)
