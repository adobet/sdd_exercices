Un peu d'algo : Troupe de jonglage
====================================



Le ministère a décidé de diversifier les compétences des étudiants en IUT en imposant une épreuve de cirque.
Désormais, les étudiants en informatique auront une épreuve de jonglage.

  .. image :: les_jongleurs.jpg

En ce moment même, un groupe d'étudiants/artistes novices est aligné sur scène pour une représentation. 
Malheureusement, aucun d'entre eux n'est très confiant dans ses comptétences de jonglage.

Ainsi, dès qu'il en a la possibilité, chaque étudiant essaie de diminuer le nombre de balles en sa possession pour réduire la difficulté de l'épreuve.
Chaque fois qu'un jongleur a deux balles, ou plus, en sa possession, il lance une balle à chacun de ses voisins.
Dans le cas où un jongleur n'a pas de voisin dans une certaine direction, il jette simplement la balle hors de la scène.
Tous les jongleurs lancent leurs balles en même temps.
La représentation prend fin quand tous les jongleurs ont une seule balle en main, ou moins.

Le schéma ci-dessous illustre un exemple de représentation avec un groupe de 8 étudiants. La représentation prend fin à l'étape 3.

  .. image :: les_jongleurs.png


Vous êtes membre du public, et pas très captivé par cette performance. 
Cependant, vous vous demandez combien de balles il restera à chacun des jongleurs à la fin du spectacle.


Vous décider d'écrire une fonction ``jonglage`` qui prend en paramètre une liste qui représente le nombre de balles de chaque étudiant du groupe au début de la représentation.
Cette fonction renverra une liste représentant le nombre de balles de chaque étudiant à la fin de la représentation.

Avec l'exemple illustré ci-dessus :

.. code::

    >>> exemple = [1, 2, 1, 0, 0, 2, 1, 2]
    >>> jonglage(exemple)
    [1, 0, 1, 1, 1, 1, 1, 1]

.. easypython:: les_jongleurs.py
   :language: python
   :uuid: 1231313
   

*Inspiré d'un exercice de ICPC Live Archive*

