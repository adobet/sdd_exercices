entrees_visibles=[
        ([1, 2, 1, 0, 0, 2, 1, 2]),
        ([0, 0,0,1,1,1,2,2,2,0,0,0,2,2,2,1,1,1,2,2,2,0,0,1]),
]
entrees_invisibles=[
        ([5,4,2,3,6,8,7]),
        ([0,2,3,4,5,6,2,1,5,6,2,1,4,8,21,4,4,1]),
        ([1, 12, 23, 34, 55, 6]),
]

def suite(liste):
  for n in liste:
    if n>=2:
      return True
  return False


@solution
def jonglage(liste):
    while suite(liste):
        l = liste.copy()
        for i in range(len(liste)):
            passe = 0 if liste[i] < 2 else 2
            recoit = 1 if i > 0 and liste[i-1] > 1 else 0
            recoit += 1 if i < len(liste)-1 and liste[i+1] > 1 else 0
            l[i] = liste[i] - passe + recoit
        liste = l.copy()
    return liste


