Ce qu'il faut retenir cette semaine
=====================================

Structures de données : listes et tuples

Données mutables et immutables

Représentation de la mémoire

Fonctionnalités à connaitre : ajouter(append), accéder, modifier, trier et max

Approche de la notion de complexité (?)


.. raw:: latex

   \newpage
