Ce qu'il faut retenir
=======================

Structures de données : ensembles et dictionnaires

Données mutables et immutables
Représentation de la mémoire

Fonctionnalités à connaitre : ajouter(add ou put), accéder, modifier, trier et max


Approche de la notion de complexité (?) comparer la vitesse du ``in`` dans une liste et dans un ensemble



.. raw:: latex

   \newpage
