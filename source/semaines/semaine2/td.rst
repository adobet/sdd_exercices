.. raw:: html

   <span class="feuille_exos"></span>


Exercices du TD1
=================

.. toctree::
   :maxdepth: 2
   :numbered: 1
   :glob:
   
   td/*

.. raw:: latex

   \newpage
