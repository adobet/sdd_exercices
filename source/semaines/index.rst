.. EasySphinx documentation master file, created by
   sphinx-quickstart on Thu Oct 20 12:10:31 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Semaine 1
=========

.. toctree::
   :maxdepth: 2
   :numbered: 0
   :glob:
   
   semaine1/*


Semaine 2
=========

.. toctree::
   :maxdepth: 2
   :numbered: 0
   :glob:
   
   semaine2/*


Semaine 3
=========

.. toctree::
   :maxdepth: 2
   :numbered: 0
   :glob:
   
   semaine3/*

Semaine 4
=========

.. toctree::
   :maxdepth: 2
   :numbered: 0
   :glob:
   
   semaine4/*

Semaine 5
=========

.. toctree::
   :maxdepth: 2
   :numbered: 0
   :glob:
   
   semaine5/*

Semaine 6
=========

.. toctree::
   :maxdepth: 2
   :numbered: 0
   :glob:
   
   semaine6/*

Semaine 7
=========

.. toctree::
   :maxdepth: 2
   :numbered: 0
   :glob:
   
   semaine7/*

Semaine 8
=========

.. toctree::
   :maxdepth: 2
   :numbered: 0
   :glob:
   
   semaine8/*

