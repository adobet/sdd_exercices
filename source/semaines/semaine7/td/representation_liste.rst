.. raw:: html

   <span class="exo"></span>


Représentation en mémoire (1)
------------------------------

1. Représenter l'état de la mémoire après l'exécution du code suivant :

.. code-block:: python

   liste1 = [ 'veau', 'vache', 'cochon', 'couvée']
   liste2 = liste1
   liste2 [3] = 'chèvre'


2. Représenter l'état de la mémoire après l'exécution de la ligne indiquée, puis préciser l'affichage provoqué par la dernière ligne.


.. code-block:: python

   def metUneChevre(liste):
      if len(liste) > 3:
         liste[3] = 'chèvre' #ici
         
   revesPerrette = [ 'veau', 'vache', 'cochon', 'couvée']
   metUneChevre(revesPerrette)
   print(revesPerrette) # à préciser

1. Représenter l'état de la mémoire juste avant de quitter la ligne indiquée.


.. code-block:: python

   def metUneChevre(liste):
      if len(liste) > 3:
         liste[3] = 'chèvre' #ici
         
   def insereUnZebu(animaux):
      if len(animaux) > 1:
         animaux[0] = animaux[1]
         animaux[1] = 'zébu'
         
   rp = ['veau', 'vache', 'cochon', 'couvée']
   insereUnZebu(rp)
   metUneChevre(rp)
   print(rp)
   
