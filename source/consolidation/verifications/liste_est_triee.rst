La liste est-elle triée ?
---------------------------

Écrire [#f1]_ une fonction ```liste_est_triee()``` qui vérifie qu'une liste de nombres est correctement triée par ordre croissant.

.. easypython:: liste_est_triee.py
   :language: python
   :uuid: 1231313


.. rubric:: Footnotes 

.. [#f1] les yeux fermés en moins de 40 secondes
