:orphan:


Echauffement
========================

.. toctree::
   :maxdepth: 2
   :numbered: 1
   :glob:
   
   elementaire/*



Des conditionnelles
========================

.. toctree::
   :maxdepth: 2
   :numbered: 1
   :glob:
   
   conditionnelles/*
   


Fonctions de cumul
========================

.. toctree::
   :maxdepth: 2
   :numbered: 1
   :glob:
   
   cumuls/*


Recherches d'extremums
===========================

.. toctree::
   :maxdepth: 2
   :numbered: 1
   :glob:
   
   extremums/*



Fonctions de vérification
===========================

.. toctree::
   :maxdepth: 2
   :numbered: 1
   :glob:
   
   verifications/*
   
