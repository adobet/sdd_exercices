Somme partielle
-----------------

Écrire une fonction ``somme_partielle(liste, n)`` qui prend en paramètre une liste de nombres et un nombre, et renvoie la somme des n premiers nombres de la liste.


.. easypython:: somme_partielle.py
   :language: python
   :uuid: 1231313

