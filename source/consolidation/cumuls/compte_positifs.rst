Combien de nombres positifs ?
---------------------------------

Écrire une fonction ``compte_positif()`` qui prend en paramètre une liste de nombres et renvoie le nombre de'éléments positifs de la liste.


.. easypython:: compte_positifs.py
   :language: python
   :uuid: 1231313

